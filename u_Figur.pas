unit u_Figur;

{$MODE Delphi}

interface

uses LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes,
  Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, ImgList;

type
  TMatrix = array [0 .. 7, 0 .. 7] of boolean;

type
  TMatrixFarb = array [0 .. 7, 0 .. 7] of integer;





type
  TFigur = class
  private
    FGelaufen: boolean;
    FType: string;
    FFarbe: integer;
    FX: integer;
    FY: integer;
    FLetzterZug: integer;
    FBauerdoppelzug: boolean;
  public
   // function Bewegen(Figur: TFigur; FigurenMatrix: TMatrix;
   //   Farben: TMatrixFarb): TMatrix;
  published
    property Gelaufen: boolean read FGelaufen write FGelaufen;
    property Typ: string read FType write FType;
    property Farbe: integer read FFarbe write FFarbe;
    property x: integer read FX write FX;
    property y: integer read FY write FY;
    property LetzterZug: integer read FLetzterZug write FLetzterZug;
    property Bauerdoppelzug: boolean read FBauerdoppelzug write FBauerdoppelzug;
  end;


  function Bewegen(Figur: TFigur; FigurenMatrix: TMatrix; Farben: TMatrixFarb): TMatrix;


implementation

function Bewegen(Figur: TFigur; FigurenMatrix: TMatrix;
  Farben: TMatrixFarb): TMatrix;
var
  ergebnis: TMatrix;
  x, y: integer;
  i: integer;
  r: integer;
  // Verwendung von Funktionen in Funktion, damit Parameterübergabe entfällt
  // sowie für Übersichtlichkeit im Case of --> Vermeidung von Begin und End darin.

  function BauerS(): TMatrix;
  begin
    if y + 1 <= 7 then
    begin

      if (Figur.Gelaufen = False) and (FigurenMatrix[x, y + 1] = False) and
        (FigurenMatrix[x, y + 2] = False) then
        ergebnis[x, y + 2] := True;
      if (FigurenMatrix[x, y + 1] = False) then
        ergebnis[x, y + 1] := True;
      if x + 1 <= 7 then
        if (FigurenMatrix[x + 1, y + 1] = True) and
          (Farben[x + 1, y + 1] <> Figur.Farbe) then
          ergebnis[x + 1, y + 1] := True;

      if (FigurenMatrix[x - 1, y + 1] = True) and
        (Farben[x - 1, y + 1] <> Figur.Farbe) then
        ergebnis[x - 1, y + 1] := True;
      Result := ergebnis;

    end;
  end;

  function BauerW(): TMatrix;
  begin
    if (Figur.Gelaufen = False) and (FigurenMatrix[x, y - 1] = False) and
      (FigurenMatrix[x, y - 2] = False) then
      ergebnis[x, y - 2] := True;
    if (FigurenMatrix[x, y - 1] = False) then
      ergebnis[x, y - 1] := True;
    if (FigurenMatrix[x + 1, y - 1] = True) and
      (Farben[x + 1, y - 1] <> Figur.Farbe) then
      ergebnis[x + 1, y - 1] := True;
    if (FigurenMatrix[x - 1, y - 1] = True) and
      (Farben[x - 1, y - 1] <> Figur.Farbe) then
      ergebnis[x - 1, y - 1] := True;
    Result := ergebnis;
  end;

  function Konig(): TMatrix;
  begin

    if (x - 1 >= 0) and (y + 1 <= 7) then
      if (Farben[x - 1, y + 1] <> Figur.Farbe) then
        ergebnis[x - 1, y + 1] := True;

    if (x + 1 <= 7) and (y + 1 <= 7) then
      if (Farben[x + 1, y + 1] <> Figur.Farbe) then
        ergebnis[x + 1, y + 1] := True;

    if (x - 1 >= 0) and (y - 1 >= 0) then
      if (Farben[x - 1, y - 1] <> Figur.Farbe) then
        ergebnis[x - 1, y - 1] := True;

    if (x + 1 <= 7) and (y - 1 >= 0) then
      if (Farben[x + 1, y - 1] <> Figur.Farbe) then
        ergebnis[x + 1, y - 1] := True;

    if (y + 1 <= 7) then
      if (Farben[x, y + 1] <> Figur.Farbe) then
        ergebnis[x, y + 1] := True;

    if (y - 1 >= 0) then
      if (Farben[x, y - 1] <> Figur.Farbe) then
        ergebnis[x, y - 1] := True;

    if (x - 1 >= 0) then
      if (Farben[x - 1, y] <> Figur.Farbe) then
        ergebnis[x - 1, y] := True;

    if (x + 1 <= 7) then
      if (Farben[x + 1, y] <> Figur.Farbe) then
        ergebnis[x + 1, y] := True;

    Result := ergebnis;

  end;

  function Laufer(): TMatrix;
  var
    i, j, ende: integer;
  begin

    ende := 2;
    i := x + 1;
    j := y + 1;
    while (i <= 7) and (j <= 7) do
    begin
      if ((Farben[i, j] <> Figur.Farbe)) and (ende <> 3) then
      begin
        ergebnis[i, j] := True;
        if FigurenMatrix[i, j] then
          ende := 3;
      end
      else
        ende := 3;
      Inc(i);
      Inc(j);
    end;

    ende := 2;
    i := x - 1;
    j := y - 1;
    while (i >= 0) and (j >= 0) do
    begin
      if ((Farben[i, j] <> Figur.Farbe)) and (ende <> 3) then
      begin
        ergebnis[i, j] := True;
        if FigurenMatrix[i, j] then
          ende := 3;
      end
      else
        ende := 3;
      Dec(i);
      Dec(j);
    end;

    ende := 2;
    i := x - 1;
    j := y + 1;
    while (i >= 0) and (j <= 7) do
    begin
      if ((Farben[i, j] <> Figur.Farbe)) and (ende <> 3) then
      begin
        ergebnis[i, j] := True;
        if FigurenMatrix[i, j] then
          ende := 3;
      end
      else
        ende := 3;
      Dec(i);
      Inc(j);
    end;

    ende := 2;
    i := x + 1;
    j := y - 1;
    while (i <= 7) and (j >= 0) do
    begin
      if ((Farben[i, j] <> Figur.Farbe)) and (ende <> 3) then
      begin
        ergebnis[i, j] := True;
        if FigurenMatrix[i, j] then
          ende := 3;
      end
      else
        ende := 3;
      Inc(i);
      Dec(j);
    end;

    Result := ergebnis;

  end;

  function Springer(): TMatrix;

  begin

    if (x - 2 >= 0) and (y + 1 <= 7) then
      if (Farben[x - 2, y + 1] <> Figur.Farbe) then
        ergebnis[x - 2, y + 1] := True;

    if (x - 2 >= 0) and (y - 1 >= 0) then
      if (Farben[x - 2, y - 1] <> Figur.Farbe) then
        ergebnis[x - 2, y - 1] := True;

    if (x + 2 <= 7) and (y - 1 >= 0) then
      if (Farben[x + 2, y - 1] <> Figur.Farbe) then
        ergebnis[x + 2, y - 1] := True;

    if (x + 2 <= 7) and (y + 1 <= 7) then
      if (Farben[x + 2, y + 1] <> Figur.Farbe) then
        ergebnis[x + 2, y + 1] := True;

    if (x + 1 <= 7) and (y + 2 <= 7) then
      if (Farben[x + 1, y + 2] <> Figur.Farbe) then
        ergebnis[x + 1, y + 2] := True;

    if (x - 1 >= 0) and (y + 2 <= 7) then
      if (Farben[x - 1, y + 2] <> Figur.Farbe) then
        ergebnis[x - 1, y + 2] := True;

    if (x + 1 <= 7) and (y - 2 >= 0) then
      if (Farben[x + 1, y - 2] <> Figur.Farbe) then
        ergebnis[x + 1, y - 2] := True;

    if (x - 1 >= 0) and (y - 2 >= 0) then
      if (Farben[x - 1, y - 2] <> Figur.Farbe) then
        ergebnis[x - 1, y - 2] := True;

    Result := ergebnis;

  end;

  function Turm(): TMatrix;
  var
    i, ende: integer;
  begin

    ende := 2;
    i := x;
    while i <> 0 do
    begin

      Dec(i);
      if ((Farben[i, y] <> Figur.Farbe)) and (ende <> 3) then
      begin
        ergebnis[i, y] := True;
        if FigurenMatrix[i, y] then
          ende := 3;
      end
      else
        ende := 3;
    end;

    ende := 2;
    i := x;
    while i <> 7 do
    begin
      Inc(i);
      if (Farben[i, y] <> Figur.Farbe) and (ende <> 3) then
      begin
        ergebnis[i, y] := True;
        if FigurenMatrix[i, y] then
          ende := 3;
      end
      else
        ende := 3;
    end;

    ende := 2;
    i := y;
    while i <> 0 do
    begin
      Dec(i);
      if (Farben[x, i] <> Figur.Farbe) and (ende <> 3) then
      begin
        ergebnis[x, i] := True;
        if FigurenMatrix[x, i] then
          ende := 3;
      end
      else
        ende := 3;
    end;

    ende := 2;
    i := y;
    while i <> 7 do
    begin
      Inc(i);
      if (Farben[x, i] <> Figur.Farbe) and (ende <> 3) then
      begin
        ergebnis[x, i] := True;
        if FigurenMatrix[x, i] then
          ende := 3;
      end
      else
        ende := 3;
    end;

    Result := ergebnis;
  end;

  function Dame(): TMatrix;
  var
    i, j: integer;
    erg1, erg2: TMatrix;
  begin
    for i := 0 to 7 do
      for j := 0 to 7 do
      begin
        erg1[i, j] := False;
        erg2[i, j] := False;
      end;
    erg1 := Turm;
    erg2 := Laufer;

    for i := 0 to 7 do
      for j := 0 to 7 do
      begin
        if erg1[i, j] = True then
          ergebnis[i, j] := True;
        if erg2[i, j] = True then
          ergebnis[i, j] := True;
      end;

    Result := ergebnis;

  end;

  // Beginn der Funktion Bewegen
begin

  x := Figur.x;
  y := Figur.y;
  for i := 0 to 7 do
    for r := 0 to 7 do
      ergebnis[i, r] := False;

  case Figur.Typ[1] of
    'B':
    begin
      if Figur.Farbe = 0 then
        Result := BauerS()
      else
        Result := BauerW();
    end;
    'D':
      Result := Dame();
    'K':
      Result := Konig();
    'L':
      Result := Laufer();
    'S':
      Result := Springer();
    'T':
      Result := Turm();
  end;

end;

end.
