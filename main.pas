unit Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus, Grids, ExtCtrls,
  StdCtrls, Buttons, u_Figur, Types, Hilfe, NeuesSpiel;

type

  { TfrmSchach }

  TfrmSchach = class(TForm)
    Beenden1: TMenuItem;
    Button1: TButton;
    DrawGrid1: TDrawGrid;
    ImageList1: TImageList;
    ImageList2: TImageList;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    lblZeitgrenze: TLabel;
    lblZeitW: TLabel;
    Label4: TLabel;
    lblZeitS: TLabel;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    N1: TMenuItem;
    NeuesSpiel1: TMenuItem;
    NeuesSpielen1: TMenuItem;
    OpenDialog1: TOpenDialog;
    PaintBox1: TPaintBox;
    Panel1: TPanel;
    Panel2: TPanel;
    SaveDialog1: TSaveDialog;
    Spielladen1: TMenuItem;
    Spielspeichern1: TMenuItem;
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure DrawGrid1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure NeuesSpielen1Click(Sender: TObject);
    procedure Beenden1Click(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure Spielspeichern1Click(Sender: TObject);
    procedure Spielladen1Click(Sender: TObject);
    procedure DrawGrid1DrawCell(Sender: TObject; aCol, aRow: integer;
      aRect: TRect; aState: TGridDrawState);
    procedure Timer1Timer(Sender: TObject);
  private
  public
  end;

procedure PBResetgame(Zeit: integer);


var
  frmSchach: TfrmSchach;

implementation

{$R *.lfm}


type
  TSchachbrett = array [0..7, 0..7] of TFigur;

var
  Zugnr, ZeitS, ZeitW, ZeitGrenze: integer;
  Zug: boolean;
  Auswahl: TFigur;
  Zuge: TMatrix;
  Schachbrett: TSchachbrett;


function NeustartDialog(frmSchach: TfrmSchach; Aufrufstatus: integer): boolean;
var
  Sieger: string;
begin

  if Zug = False then
    Sieger := 'Weiß';
  if Zug = True then
    Sieger := 'Schwarz';

  with TTaskDialog.Create(frmSchach) do
    try
      case Aufrufstatus of
        0:
          Title := 'Schachmatt!' + #13#10 + Sieger + ' hat gewonnen!';
        1:
          Title := 'Wirklich neustarten?' + #13#10 +
            'Alle nicht gespeicherten Änderungen gehen verloren!';
        2:
          Title := 'Wirklich beenden?' + #13#10 +
            'Alle nicht gespeicherten Änderungen gehen verloren!';
        3:
          Title := 'Spiel laden?' + #13#10 +
            'Alle nicht gespeicherten Änderungen gehen verloren!';
        4:
          Title := 'Zeit abgelaufen!' + #13#10 + Sieger + ' hat gewonnen!';
        5:
          Title:='Patt - Unentschieden!'+#13#10 +'Niemand hat gewonnen.';
      end;
      Caption := '';
      CommonButtons := [];
      with TTaskDialogButtonItem(Buttons.Add) do
      begin
        case Aufrufstatus of
          0 .. 1, 4,5:
            Caption := 'Neustarten.';
          2:
            Caption := 'Beenden.';
          3:
            Caption := 'Spiel laden.';
        end;
        ModalResult := mrOk;
      end;
      with TTaskDialogButtonItem(Buttons.Add) do
      begin
        if Aufrufstatus = 4 then
          Caption := 'Zeitgrenze deaktivieren'
        else
          Caption := 'Noch nicht!';
        ModalResult := mrCancel;
      end;
      Flags := [tfUseCommandLinks];
      MainIcon := tdiNone;
      if Execute then
        if ModalResult = mrOk then
          Result := True
        else
          Result := False;

    finally
      Free;
    end;

end;

function Bauerauswahl(frmSchach: TfrmSchach): integer;
begin
  with TTaskDialog.Create(frmSchach) do
    try
      Title := 'In was soll der Bauer umgewandelt werden?';
      Caption := '';
      CommonButtons := [];
      with TTaskDialogButtonItem(Buttons.Add) do
      begin
        Caption := 'Springer';
        ModalResult := 1;
      end;
      with TTaskDialogButtonItem(Buttons.Add) do
      begin
        Caption := 'Läufer';
        ModalResult := 2;
      end;
      with TTaskDialogButtonItem(Buttons.Add) do
      begin
        Caption := 'Turm';
        ModalResult := 3;
      end;
      with TTaskDialogButtonItem(Buttons.Add) do
      begin
        Caption := 'Dame';
        ModalResult := 4;
      end;
      Flags := [tfUseCommandLinks];
      MainIcon := tdiNone;
      if Execute then
        Result := ModalResult;

    finally
      Free;
    end;
end;

function possible(KPos, FPos: Tpoint; Feld, LokZuge: TMatrix;
  Farben: TMatrixFarb): boolean;
var
  i, r: integer;
  Figur: TFigur;
  Schach: boolean;
  Hilfzuge: TMatrix;
begin
  Schach := False;

  for i := 0 to 7 do
    for r := 0 to 7 do
    begin
      Hilfzuge[i, r] := False;
    end;

  for i := 0 to 7 do
    for r := 0 to 7 do
    begin
      if Schachbrett[i, r] is TFigur then
      begin
        // Überprüfung der möglichen gegnerischen Züge
        Figur := TFigur.Create;
        Figur := Schachbrett[i, r] as TFigur;
        if ((Figur.Farbe = 0) and (Zug = False)) or
          ((Figur.Farbe = 1) and (Zug = True)) then
        begin

          Hilfzuge := Bewegen(Figur, Feld, Farben);
          if Hilfzuge[KPos.X, KPos.Y] = True then
          begin
            Schach := True;

            if (LokZuge[Figur.X, Figur.Y] = True) and (FPos.X = Figur.X) and
              (FPos.Y = Figur.Y) then
              Schach := False;

          end;
        end;
        Figur := nil;
        Figur.Free;
      end;

    end;

  Result := not Schach;

end;

procedure Resetgame();
var
  r, i: integer;
  Figur: TFigur;
  Typ: string;
begin

  for i := 0 to 7 do
    for r := 0 to 7 do
    begin
      Schachbrett[i, r] := nil;
    end;

  for r := 0 to 1 do
    for i := 0 to 7 do
    begin
      case i of
        0:
          Typ := 'Turm';
        1:
          Typ := 'Springer';
        2:
          Typ := 'Laufer';
        3:
          Typ := 'Dame';
        4:
          Typ := 'Konig';
        5:
          Typ := 'Laufer';
        6:
          Typ := 'Springer';
        7:
          Typ := 'Turm';
      end;

      if r = 1 then
      begin
        Typ := 'Bauer';
      end;

      Figur := TFigur.Create;
      Figur.Gelaufen := False;
      Figur.Farbe := 0;
      Figur.Typ := Typ;
      Figur.X := i;
      Figur.Y := r;
      // En Passant
      Figur.LetzterZug := 0;
      Figur.Bauerdoppelzug := False;
      Schachbrett[i, r] := Figur;

      Figur := nil;
      Figur.Free;
    end;

  for r := 6 to 7 do
    for i := 0 to 7 do
    begin
      case i of
        0:
          Typ := 'Turm';
        1:
          Typ := 'Springer';
        2:
          Typ := 'Laufer';
        3:
          Typ := 'Dame';
        4:
          Typ := 'Konig';
        5:
          Typ := 'Laufer';
        6:
          Typ := 'Springer';
        7:
          Typ := 'Turm';
      end;

      if r = 6 then
      begin
        Typ := 'Bauer';
      end;

      Figur := TFigur.Create;
      Figur.Gelaufen := False;
      Figur.Typ := Typ;
      Figur.Farbe := 1;
      Figur.X := i;
      Figur.Y := r;
      Figur.LetzterZug := 0;
      Figur.Bauerdoppelzug := False;
      Schachbrett[i, r] := Figur;
      Figur := nil;
      Figur.Free;
    end;
  Zugnr := 0;
  Zug := True;
  Auswahl := TFigur.Create;
  Auswahl.X := 99;
  Auswahl.Y := 99;
  ZeitS := 0;
  ZeitW := 0;

  if Zug then
    frmschach.Panel1.Caption := 'Weiß ist am Zug!'
  else
    frmschach.Panel1.Caption := 'Schwarz ist am Zug!';


  frmschach.lblZeitgrenze.Caption := IntToStr(ZeitGrenze div 1000) + ' s';

end;

procedure PbResetgame(Zeit: integer);
begin
  Resetgame();
  Zeitgrenze := Zeit * 1000;

  frmschach.lblZeitgrenze.Caption := IntToStr(ZeitGrenze div 1000) + ' s';
end;

procedure TfrmSchach.Beenden1Click(Sender: TObject);
begin
  if NeustartDialog(frmSchach, 2) then
    Close;

end;

procedure TfrmSchach.PaintBox1Paint(Sender: TObject);
var
  I: integer;
  B: char;
begin
  with Paintbox1.Canvas do
  begin
    brush.color := $00ACACAC;
    fillrect(0, 0, Paintbox1.Height, Paintbox1.Width);
    Pen.Color := clblack;
    Pen.Width := 2;
    Brush.Style := bsClear;
    Font.Height := 25;
    for i := 0 to 7 do
    begin
      case i of
        0: B := 'A';
        1: B := 'B';
        2: B := 'C';
        3: B := 'D';
        4: B := 'E';
        5: B := 'F';
        6: B := 'G';
        7: B := 'H';
      end;
      Textout(Drawgrid1.left + 26 + (64 * i), Drawgrid1.top - 26, B);
      Textout(Drawgrid1.left + 26 + (64 * i), Drawgrid1.Height + 36, B);
      Textout(Drawgrid1.left - 20, Drawgrid1.top + 20 + (64 * i), IntToStr(I + 1));
      Textout(Drawgrid1.left + Drawgrid1.Width + 10, Drawgrid1.top +
        20 + (64 * i), IntToStr(I + 1));
    end;
    Font.Height := 50;
    Font.color := clred;
    TextOut((Drawgrid1.ClientWidth - 150) div 2, (Drawgrid1.ClientHeight) div
      2, 'Spiel Pausiert!');
  end;
end;

procedure TfrmSchach.FormCreate(Sender: TObject);
var
  i: integer;
  r: integer;
begin
  for i := 0 to 7 do
    for r := 0 to 7 do
      Zuge[r, i] := False;

  resetgame();

  Auswahl := TFigur.Create;
  Auswahl.X := 99;
  Auswahl.Y := 99;
  frmschach.BorderStyle := bssingle;
  ZeitS := 0;
  ZeitW := 0;
  ZeitGrenze := 0;

end;


procedure TfrmSchach.MenuItem1Click(Sender: TObject);
begin
  frmhilfe := TFrmHilfe.Create(nil);
  Frmhilfe.Show;

end;

procedure TfrmSchach.DrawGrid1DrawCell(Sender: TObject; aCol, aRow: integer;
  aRect: TRect; aState: TGridDrawState);
var
  Figur: TFigur;
  ID: integer;
begin
  with DrawGrid1 do
  begin
    canvas.Brush.Style := bsSolid;
    if odd(aCol + aRow) then
      canvas.Brush.Color := clWhite
    else
      canvas.Brush.Color := clGray;

    canvas.FillRect(arect);

  end;
  // Figuren zeichnen falls imagelists gegeben
  ID := 0;
  if (ImageList1 <> nil) and (ImageList2 <> nil) then
  begin

    Figur := TFigur.Create;
    if Schachbrett[aCol, aRow] is TFigur then
    begin
      Figur := Schachbrett[aCol, aRow] as TFigur;
      case Figur.Typ[1] of
        'B':
          ID := 0;
        'D':
          ID := 1;
        'K':
          ID := 2;
        'L':
          ID := 3;
        'S':
          ID := 4;
        'T':
          ID := 5;
      end;
      if Figur.Farbe = 1 then
        ImageList2.Draw(DrawGrid1.canvas, arect.left, arect.Top, ID);
      if Figur.Farbe = 0 then
        ImageList1.Draw(DrawGrid1.canvas, arect.left, arect.Top, ID);
    end;

  end;

  if Zuge[aCol, aRow] = True then
  begin
    DrawGrid1.canvas.Brush.Color := clgreen;
    DrawGrid1.canvas.Brush.Style := bsSolid;
    DrawGrid1.canvas.Ellipse(arect.CenterPoint.X - 8, arect.CenterPoint.Y - 8,

      arect.CenterPoint.X + 8,
      arect.CenterPoint.Y + 8);

  end;
end;

procedure TfrmSchach.Timer1Timer(Sender: TObject);
begin

  if Zug = True then
  begin
    Inc(ZeitW, 100);

    lblZeitW.Caption := FloatToStrF(ZeitW / 1000, fffixed, 10, 2) + ' s';

    if (ZeitW >= Zeitgrenze) and (Zeitgrenze > 0) then
    begin
      timer1.Enabled := False;
      if NeustartDialog(frmSchach, 4) then
      begin

        frmNeuesSpiel := TfrmNeuesSpiel.Create(nil);
        frmNeuesSpiel.ShowModal;
        Drawgrid1.Invalidate;

      end
      else
        Zeitgrenze := 0;
    end;

  end
  else
  begin

    Inc(ZeitS, 100);
    lblZeitS.Caption := FloatToStrF(ZeitS / 1000, fffixed, 10, 2) + ' s';

    if (ZeitS >= Zeitgrenze) and (Zeitgrenze > 0) then
    begin
      timer1.Enabled := False;
      if NeustartDialog(frmSchach, 4) then
      begin
        frmNeuesSpiel := TfrmNeuesSpiel.Create(nil);
        frmNeuesSpiel.ShowModal;
        Drawgrid1.Invalidate;
      end
      else
        Zeitgrenze := 0;
    end;

  end;

end;

procedure TfrmSchach.NeuesSpielen1Click(Sender: TObject);
begin
  timer1.Enabled := False;
  if NeustartDialog(frmSchach, 1) then
  begin
    frmNeuesSpiel := TfrmNeuesSpiel.Create(nil);
    frmNeuesSpiel.ShowModal;

    Drawgrid1.Invalidate;
  end;
end;

procedure TfrmSchach.Spielladen1Click(Sender: TObject);
var
  Savegame: TStringList;
  i, r, j: integer;
  Figur: TFigur;
  S: string;

begin
  if Timer1.Enabled then
    Timer1.Enabled := False;
  if NeustartDialog(frmSchach, 3) then
  begin

    if OpenDialog1.Execute then
    begin

      if ExtractFileExt(OpenDialog1.filename) = '.ChessGame' then
      begin

        Savegame := TStringList.Create;
        Savegame.LoadFromFile(OpenDialog1.filename);
        if Savegame.Count = 69 then
        begin

          j := 0;

          for i := 0 to 7 do
            for r := 0 to 7 do
              Schachbrett[i, r] := nil;

          i := 0;
          r := 0;

          for j := 0 to 63 do
          begin
            S := Savegame[j];

            if S[1] <> '0' then
            begin
              Figur := TFigur.Create;
              Figur.Typ := S[1];
              Figur.Farbe := StrToInt(S[2]);
              Figur.LetzterZug := StrToInt(S[3]);
              Figur.Gelaufen := StrToBool(S[4]);
              Figur.Bauerdoppelzug := StrToBool(S[5]);
              Figur.X := i;
              Figur.Y := r;
              Schachbrett[i, r] := Figur;
              Figur := nil;

              Figur := nil;
              Figur.Free;

            end;
            Inc(i);
            if i = 8 then
            begin
              Inc(r);
              i := 0;
            end;

          end;
          Zugnr := StrToInt(Savegame[64]);
          Zug := StrToBool(Savegame[65]);
          ZeitW := StrToInt(Savegame[66]);
          ZeitS := StrToInt(Savegame[67]);
          ZeitGrenze := StrToInt(Savegame[68]);
          lblZeitW.Caption := FloatToStrF(ZeitW / 1000, fffixed, 10, 2) + ' s';
          lblZeitS.Caption := FloatToStrF(ZeitS / 1000, fffixed, 10, 2) + ' s';
          lblZeitgrenze.Caption := IntToStr(ZeitGrenze div 1000) + ' s';
          Drawgrid1.Invalidate;

          if Zug then
            Panel1.Caption := 'Weiß ist am Zug!'
          else
            Panel1.Caption := 'Schwarz ist am Zug!';

          Savegame.Free;
        end
        else
          ShowMessage('Datei konnte nicht geladen werden!');
      end
      else
        ShowMessage('Datei konnte nicht geladen werden!');
    end;
  end;

end;

procedure TfrmSchach.Spielspeichern1Click(Sender: TObject);
var
  Savegame: TStringList;
  i, r: integer;
  Figur: TFigur;
  S: string;
begin
  if Timer1.Enabled then
    timer1.Enabled := False;
  Savegame := TStringList.Create;
  Figur := TFigur.Create;

  for r := 0 to 7 do
    for i := 0 to 7 do
    begin

      if Schachbrett[i, r] is TFigur then
      begin
        Figur := Schachbrett[i, r] as TFigur;
        S := Figur.Typ[1] + IntToStr(Figur.Farbe) + IntToStr(Figur.LetzterZug);
        if Figur.Gelaufen = True then
          S := S + '1'
        else
          S := S + '0';
        if Figur.Bauerdoppelzug = True then
          S := S + '1'
        else
          S := S + '0';
        Savegame.Append(S);
      end
      else
        Savegame.Append('0');
    end;
  Savegame.Append(IntToStr(Zugnr));
  if Zug then
    Savegame.Append('1')
  else
    Savegame.Append('0');
  Savegame.Append(IntToStr(ZeitW));
  Savegame.Append(IntToStr(ZeitS));
  Savegame.Append(IntToStr(ZeitGrenze));

  SaveDialog1.filename := 'Schach Spiel - ' + DateToStr(Date);
  if SaveDialog1.Execute then
  begin
    Savegame.SaveToFile(SaveDialog1.filename);
  end;

  Savegame.Free;
  Figur := nil;
  Figur.Free;
end;

function MlgZuge(Zeile, Reihe: integer): TMatrix;
var
  i, r: integer;
  Lokzuge, Figurenfeld, HilfFigurenFeld, HilfZuge: TMatrix;
  Farbfeld, HilfFarbFeld: TMatrixFarb;
  Figur, HilfFigur, HHilfFigur: TFigur;
  KPos, FPos: TPoint;
  Rochade: boolean;

begin
  Auswahl := Schachbrett[Zeile, Reihe] as TFigur;

  // Standardwert (Nicht belegt) setzen bei Feldern
  for r := 0 to 7 do
    for i := 0 to 7 do
    begin
      Figurenfeld[i, r] := False;
      Farbfeld[i, r] := 2;
      LokZuge[i, r] := False;
    end;

  // Überprüfen ob Felder belegt sind.
  for r := 0 to 7 do
    for i := 0 to 7 do
    begin
      if Schachbrett[i, r] <> nil then
      begin
        Figur := Schachbrett[i, r];

        // Erkennen ob König der Farbe
        if (Figur.Typ[1] = 'K') and (((Figur.Farbe = 1) and (Zug = False)) or
          ((Figur.Farbe = 0) and (Zug = True))) then
        begin
          KPos.X := i;
          KPos.Y := r;
        end;

        Figurenfeld[i, r] := True;
        if Figur.Farbe = 0 then
          Farbfeld[i, r] := 0
        else
          Farbfeld[i, r] := 1;
      end
      else
        Farbfeld[i, r] := 2;

    end;

  LokZuge := Bewegen(Auswahl, Figurenfeld, Farbfeld);

  // En Passant
  if (Auswahl.Typ[1] = 'B') then
  begin
    if (Auswahl.Farbe = 1) and (Auswahl.Y = 3) then
    begin
      if (Auswahl.X > 0) then
        if Schachbrett[Auswahl.X - 1, Auswahl.Y] is TFigur then
        begin
          HilfFigur := Schachbrett[Auswahl.X - 1, Auswahl.Y] as TFigur;
          if (HilfFigur.Typ[1] = 'B') and (HilfFigur.Farbe <>
            Auswahl.Farbe) and (HilfFigur.Bauerdoppelzug = True) and
            (Zugnr - HilfFigur.LetzterZug = 1) then
            LokZuge[Auswahl.X - 1, Auswahl.Y - 1] := True;
        end;

      if (Auswahl.X < 7) then
        if Schachbrett[Auswahl.X + 1, Auswahl.Y] is TFigur then
        begin
          HilfFigur := Schachbrett[Auswahl.X + 1, Auswahl.Y] as TFigur;
          if (HilfFigur.Typ[1] = 'B') and (HilfFigur.Farbe <>
            Auswahl.Farbe) and (HilfFigur.Bauerdoppelzug = True) and
            (Zugnr - HilfFigur.LetzterZug = 1) then
            LokZuge[Auswahl.X + 1, Auswahl.Y - 1] := True;
        end;

    end;

    if (Auswahl.Farbe = 0) and (Auswahl.Y = 4) then
    begin
      if (Auswahl.X > 0) then
        if Schachbrett[Auswahl.X - 1, Auswahl.Y] is TFigur then
        begin
          HilfFigur := Schachbrett[Auswahl.X - 1, Auswahl.Y] as TFigur;
          if (HilfFigur.Typ[1] = 'B') and (HilfFigur.Farbe <>
            Auswahl.Farbe) and (HilfFigur.Bauerdoppelzug = True) and
            (Zugnr - HilfFigur.LetzterZug = 1) then
            LokZuge[Auswahl.X - 1, Auswahl.Y + 1] := True;
        end;

      if (Auswahl.X < 7) then
        if Schachbrett[Auswahl.X + 1, Auswahl.Y] is TFigur then
        begin
          HilfFigur := Schachbrett[Auswahl.X + 1, Auswahl.Y] as TFigur;
          if (HilfFigur.Typ[1] = 'B') and (HilfFigur.Farbe <>
            Auswahl.Farbe) and (HilfFigur.Bauerdoppelzug = True) and
            (Zugnr - HilfFigur.LetzterZug = 1) then
            LokZuge[Auswahl.X + 1, Auswahl.Y + 1] := True;
        end;
    end;

  end;

  // Mögliches "Schach" - Erkennen
  for r := 0 to 7 do
    for i := 0 to 7 do
    begin

      if LokZuge[i, r] = True then
      begin
        // FPos: Bewegungspunkt der Figur
        FPos.X := i;
        FPos.Y := r;

        HilfFigurenFeld := Figurenfeld;
        HilfFarbFeld := Farbfeld;
        Figur := Schachbrett[Auswahl.X, Auswahl.Y] as TFigur;
        HilfFigurenFeld[Auswahl.X, Auswahl.Y] := False;
        HilfFarbFeld[Auswahl.X, Auswahl.Y] := 2;
        HilfFigurenFeld[i, r] := True;
        HilfFarbFeld[i, r] := Figur.Farbe;

        if Figur.Typ[1] = 'K' then
        begin
          KPos.X := FPos.X;
          KPos.Y := FPos.Y;
        end;
        // Übergeben von veränderten Stringgrid wie Figubfeld,
        // da ansonsten Figuren falsch sind.

        if possible(KPos, FPos, HilfFigurenFeld, LokZuge, HilfFarbFeld) =
          False then
          LokZuge[i, r] := False;

      end;

    end;

  // Rochade
  if ((Figur.Typ[1] = 'K') and (Figur.Gelaufen = False)) then
  begin
    if Figur.Farbe = 0 then
    begin
      if ((Figurenfeld[1, 0] = False) and (Figurenfeld[2, 0] = False) and
        (Figurenfeld[3, 0] = False) and (Figurenfeld[0, 0] = True)) then
      begin
        HilfFigur := TFigur.Create;
        HilfFigur := Schachbrett[0, 0] as TFigur;
        if (HilfFigur.Typ[1] = 'T') and (HilfFigur.Gelaufen = False) then
        begin
          Rochade := False;
          for i := 0 to 7 do
            for r := 0 to 7 do
            begin
              if Schachbrett[r, i] is TFigur then
              begin
                HHilfFigur := TFigur.Create;
                HHilfFigur := Schachbrett[r, i] as TFigur;
                if HHilfFigur.Farbe <> Figur.Farbe then
                begin

                  Hilfzuge :=
                    Bewegen(HHilfFigur, Figurenfeld, Farbfeld);
                  if ((Hilfzuge[0, 0] = False) and
                    (Hilfzuge[1, 0] = False) and (Hilfzuge[2, 0] = False) and
                    (Hilfzuge[3, 0] = False) and (Hilfzuge[4, 0] = False)) =
                    False then
                    Rochade := False;

                end;
                HHilfFigur := nil;
                HHILffigur.Free;
              end;
            end;
          if Rochade = True then
            LokZuge[2, 0] := True;

        end;
        HilfFigur := nil;
        HIlffigur.Free;
      end;


      if ((Figurenfeld[5, 0] = False) and (Figurenfeld[6, 0] = False) and
        (Figurenfeld[7, 0] = True)) then
      begin
        HilfFigur := TFigur.Create;
        HilfFigur := Schachbrett[7, 0] as TFigur;
        if (HilfFigur.Typ[1] = 'T') and (HilfFigur.Gelaufen = False) then
          Rochade := True;
        for i := 0 to 7 do
          for r := 0 to 7 do
          begin
            if Schachbrett[r, i] is TFigur then
            begin
              HHilfFigur := TFigur.Create;
              HHilfFigur := Schachbrett[r, i] as TFigur;
              if HHilfFigur.Farbe <> Figur.Farbe then
              begin

                Hilfzuge :=
                  Bewegen(HHilfFigur, Figurenfeld, Farbfeld);
                if ((Hilfzuge[7, 0] = False) and
                  (Hilfzuge[6, 0] = False) and (Hilfzuge[5, 0] = False) and
                  (Hilfzuge[4, 0] = False)) = False then
                  Rochade := False;

              end;

              HHilfFigur := nil;
              HHilfFigur.Free;
            end;
          end;
        if Rochade = True then
          LokZuge[6, 0] := True;


        HilfFigur := nil;
        HilfFigur.Free;
      end;

    end;
    if Figur.Farbe = 1 then
    begin
      if ((Figurenfeld[1, 7] = False) and (Figurenfeld[2, 7] = False) and
        (Figurenfeld[3, 7] = False) and (Figurenfeld[0, 7] = True)) then
      begin
        HilfFigur := TFigur.Create;
        HilfFigur := Schachbrett[0, 7] as TFigur;
        if (HilfFigur.Typ[1] = 'T') and (HilfFigur.Gelaufen = False) then
        begin
          Rochade := True;
          for i := 0 to 7 do
            for r := 0 to 7 do
            begin
              if Schachbrett[r, i] is TFigur then
              begin
                HHilfFigur := TFigur.Create;
                HHilfFigur := Schachbrett[r, i] as TFigur;
                if HHilfFigur.Farbe <> Figur.Farbe then
                begin

                  Hilfzuge :=
                    Bewegen(HHilfFigur, Figurenfeld, Farbfeld);
                  if ((Hilfzuge[0, 7] = False) and
                    (Hilfzuge[1, 7] = False) and (Hilfzuge[2, 7] = False) and
                    (Hilfzuge[3, 7] = False) and (Hilfzuge[4, 7] = False)) =
                    False then
                    Rochade := False;

                end;
                HHilfFigur := nil;
                HHilfFigur.Free;
              end;
            end;
          if Rochade = True then
            LokZuge[2, 7] := True;

        end;
        HilfFigur := nil;
        HilfFigur.Free;
      end;

      if ((Figurenfeld[5, 7] = False) and (Figurenfeld[6, 7] = False) and
        (Figurenfeld[7, 7] = True)) then
      begin
        HilfFigur := TFigur.Create;
        HilfFigur := Schachbrett[7, 7] as TFigur;
        if (HilfFigur.Typ[1] = 'T') and (HilfFigur.Gelaufen = False) then
          Rochade := True;
        for i := 0 to 7 do
          for r := 0 to 7 do
          begin
            if Schachbrett[r, i] is TFigur then
            begin
              HHilfFigur := TFigur.Create;
              HHilfFigur := Schachbrett[r, i] as TFigur;
              if HHilfFigur.Farbe <> Figur.Farbe then
              begin

                Hilfzuge :=
                  Bewegen(HHilfFigur, Figurenfeld, Farbfeld);
                if ((Hilfzuge[7, 7] = False) and
                  (Hilfzuge[6, 7] = False) and (Hilfzuge[5, 7] = False) and
                  (Hilfzuge[4, 7] = False)) = False then
                  Rochade := False;

              end;

              HHilfFigur := nil;
              HHilfFigur.Free;
            end;
          end;
        if Rochade = True then
          LokZuge[6, 7] := True;


        HilfFigur := nil;
        HilfFigur.Free;
      end;

    end;

  end;
  Result := Lokzuge;

end;

procedure Figurbewegen(Zeile, Reihe: integer);
var
  Rochade, EnPassant: boolean;
  HilfFigur: TFigur;
begin

  Rochade := False;

  // Rochade
  if (Auswahl.Typ[1] = 'K') and (Auswahl.Gelaufen = False) then
  begin

    if (Zeile = 2) and (Reihe = 0) then
    begin
      HilfFigur := TFigur.Create;
      HilfFigur := Schachbrett[0, 0] as TFigur;
      Schachbrett[0, 0] := nil;
      Auswahl.X := 2;
      Auswahl.Y := 0;
      HilfFigur.X := 3;
      HilfFigur.Y := 0;
      Schachbrett[Zeile, Reihe] := Auswahl;
      Schachbrett[4, 0] := nil;
      Schachbrett[3, 0] := HilfFigur;
      Rochade := True;

      HilfFigur := nil;
      Hilffigur.Free;
    end;

    if (Zeile = 6) and (Reihe = 0) then
    begin
      HilfFigur := TFigur.Create;
      HilfFigur := Schachbrett[7, 0] as TFigur;
      Schachbrett[7, 0] := nil;
      Auswahl.X := 6;
      Auswahl.Y := 0;
      HilfFigur.X := 5;
      HilfFigur.Y := 0;
      Schachbrett[Zeile, Reihe] := Auswahl;
      Schachbrett[4, 0] := nil;
      Schachbrett[5, 0] := HilfFigur;
      Rochade := True;


      HilfFigur := nil;
      Hilffigur.Free;
    end;

    if (Zeile = 2) and (Reihe = 7) then
    begin
      HilfFigur := TFigur.Create;
      HilfFigur := Schachbrett[0, 7] as TFigur;
      Schachbrett[0, 7] := nil;
      Auswahl.X := 2;
      Auswahl.Y := 7;
      HilfFigur.X := 3;
      HilfFigur.Y := 7;
      Schachbrett[Zeile, Reihe] := Auswahl;
      Schachbrett[4, 7] := nil;
      Schachbrett[3, 7] := HilfFigur;
      Rochade := True;


      HilfFigur := nil;
      Hilffigur.Free;
    end;

    if (Zeile = 6) and (Reihe = 7) then
    begin
      HilfFigur := TFigur.Create;
      HilfFigur := Schachbrett[7, 7] as TFigur;
      Schachbrett[7, 7] := nil;
      Auswahl.X := 6;
      Auswahl.Y := 7;
      HilfFigur.X := 5;
      HilfFigur.Y := 7;
      Schachbrett[Zeile, Reihe] := Auswahl;
      Schachbrett[4, 7] := nil;
      Schachbrett[5, 7] := HilfFigur;
      Rochade := True;


      HilfFigur := nil;
      Hilffigur.Free;
    end;

  end;

  // Doppelzug für En Passant
  if (Auswahl.Typ[1] = 'B') and (Auswahl.Gelaufen = False) and
    ((Reihe = Auswahl.Y + 2) or (Reihe = Auswahl.Y - 2)) then
  begin
    Auswahl.Bauerdoppelzug := True;
  end;

  EnPassant := False;
  // Ausführung von En Passant
  if (Auswahl.Typ[1] = 'B') and ((Schachbrett[Zeile, Reihe] is TFigur) = False) and
    ((Zeile = Auswahl.X + 1) or (Zeile = Auswahl.X - 1)) then

  begin

    if Zeile = Auswahl.X - 1 then
    begin
      Auswahl.LetzterZug := Zugnr;
      Auswahl.Gelaufen := True;
      Schachbrett[Auswahl.X, Auswahl.Y] := nil;
      Schachbrett[Auswahl.X - 1, Auswahl.Y] := nil;
      Auswahl.X := Zeile;
      Auswahl.Y := Reihe;
      Schachbrett[Zeile, Reihe] := Auswahl;
    end;

    if Zeile = Auswahl.X + 1 then
    begin
      Auswahl.LetzterZug := Zugnr;
      Auswahl.Gelaufen := True;
      Schachbrett[Auswahl.X, Auswahl.Y] := nil;
      Schachbrett[Auswahl.X + 1, Auswahl.Y] := nil;
      Auswahl.X := Zeile;
      Auswahl.Y := Reihe;
      Schachbrett[Zeile, Reihe] := Auswahl;
    end;

    EnPassant := True;
  end;

  if (Rochade = False) and (EnPassant = False) then
  begin
    Auswahl.LetzterZug := Zugnr;
    Auswahl.Gelaufen := True;
    Schachbrett[Auswahl.X, Auswahl.Y] := nil;
    Auswahl.X := Zeile;
    Auswahl.Y := Reihe;

    if (Auswahl.Typ[1] = 'B') and ((Auswahl.Y = 7) and (Auswahl.Farbe = 0) or
      (Auswahl.Y = 0) and (Auswahl.Farbe = 1)) then
    begin
      frmschach.Timer1.Enabled := False;
      case Bauerauswahl(frmSchach) of
        1:
          Auswahl.Typ := 'Springer';
        2:
          Auswahl.Typ := 'Laufer';
        3:
          Auswahl.Typ := 'Turm';
        4:
          Auswahl.Typ := 'Dame';
      end;
      frmschach.Timer1.Enabled := True;
    end;

    Schachbrett[Zeile, Reihe] := Auswahl;
  end;

  // ZugNr erhöhen
  Inc(Zugnr);
  // Neu zeichnen
  frmschach.Drawgrid1.Invalidate;
  // Anderer Spieler ist dran.
  if Zug = True then
    Zug := False
  else
    Zug := True;

end;

procedure Schachmatt();
var
  i, r, g, h: integer;
  BSchach, Matt: boolean;
  HilfFigurenFeld, Figurenfeld, Lokzuge, Mattzuge: TMatrix;
  Figur: TFigur;
  Kpos, Fpos: TPoint;
  HilfFarbFeld, Farbfeld: TMatrixFarb;
begin

  for r := 0 to 7 do
    for i := 0 to 7 do
    begin

      Mattzuge[r, i] := False;
      if Schachbrett[i, r] is TFigur then
      begin
        Figur := Schachbrett[i, r] as TFigur;

        // Erkennen ob König der Farbe
        if (Figur.Typ[1] = 'K') and (((Figur.Farbe = 1) and (Zug = False)) or
          ((Figur.Farbe = 0) and (Zug = True))) then
        begin
          KPos.X := i;
          KPos.Y := r;
        end;

        Figurenfeld[i, r] := True;
        if Figur.Farbe = 0 then
          Farbfeld[i, r] := 0
        else
          Farbfeld[i, r] := 1;
      end
      else
      begin
        Farbfeld[i, r] := 2;
        Figurenfeld[i, r] := False;
      end;

    end;

  BSchach := False;
  for r := 0 to 7 do
    for i := 0 to 7 do
    begin
      zuge[i, r] := False;
      if Schachbrett[i, r] is TFigur then
      begin
        Figur := Schachbrett[i, r] as TFigur;
        if ((Figur.Farbe = 1) and (Zug = True)) or
          ((Figur.Farbe = 0) and (Zug = False)) then
        begin

          LokZuge := Bewegen(Figur, Figurenfeld, Farbfeld);

          if LokZuge[KPos.X, KPos.Y] = True then
          begin
            BSchach := True;
          end;

          for g := 0 to 7 do
            for h := 0 to 7 do
              LokZuge[h, g] := False;
        end;
      end;
    end;

  Matt := True;


  for r := 0 to 7 do
    for i := 0 to 7 do
      if Schachbrett[i, r] is TFigur then
      begin
        Figur := Schachbrett[i, r] as TFigur;
        if ((Figur.Farbe = 0) and (Zug = True)) or
          ((Figur.Farbe = 1) and (Zug = False)) then
        begin
          LokZuge := Bewegen(Figur, Figurenfeld, Farbfeld);
          for g := 0 to 7 do
            for h := 0 to 7 do
            begin
              if LokZuge[h, g] = True then
              begin
                // FPos: Bewegungspunkt der Figur
                FPos.X := h;
                FPos.Y := g;

                HilfFigurenFeld := Figurenfeld;
                HilfFarbFeld := Farbfeld;
                Figur := Schachbrett[Figur.X, Figur.Y] as TFigur;
                HilfFigurenFeld[Figur.X, Figur.Y] := False;
                HilfFarbFeld[Figur.X, Figur.Y] := 2;
                HilfFigurenFeld[h, g] := True;
                HilfFarbFeld[h, g] := Figur.Farbe;

                if Figur.Typ[1] = 'K' then
                begin
                  KPos.X := FPos.X;
                  KPos.Y := FPos.Y;
                end;

                if possible(KPos, FPos, HilfFigurenFeld, LokZuge,
                  HilfFarbFeld) = True then
                  Mattzuge[h, g] := True;
              end;
            end;
        end;

      end;


  for i := 0 to 7 do
    for r := 0 to 7 do
      if Mattzuge[i, r] = True then
        Matt := False;

  if Matt and BSchach then
  begin
    if NeustartDialog(frmSchach, 0) = True then
    begin
      timer1.enabled:=false;
      frmNeuesSpiel := TfrmNeuesSpiel.Create(nil);
      frmNeuesSpiel.ShowModal;
      frmschach.Drawgrid1.Invalidate;
    end;
  end
  else if BSchach and not Matt then
    ShowMessage('Schach')
  else if Matt and not BSchach then
    if NeustartDialog(frmSchach, 5) = True then
    begin
      timer1.enabled:=false;
      frmNeuesSpiel := TfrmNeuesSpiel.Create(nil);
      frmNeuesSpiel.ShowModal;
      frmschach.Drawgrid1.Invalidate;
    end;

end;



procedure TfrmSchach.DrawGrid1Click(Sender: TObject);
var
  Figur: TFigur;
  Selvalid: boolean;
  LokZuge: TMatrix;
begin

  if not Timer1.Enabled then
    timer1.Enabled := True;

  if Schachbrett[DrawGrid1.Col, DrawGrid1.Row] is TFigur then
  begin

    Figur := TFigur.Create;
    Figur := Schachbrett[DrawGrid1.Col, DrawGrid1.Row] as TFigur;

    Selvalid := False;
    if (Zug = True) and (Figur.Farbe = 0) then
      Selvalid := True;
    if (Zug = False) and (Figur.Farbe = 1) then
      Selvalid := True;

    Figur := nil;
    Figur.Free;
    // Wenn Figur nicht die vorherige ist
    if ((Auswahl.X <> DrawGrid1.Col) or (Auswahl.Y <> DrawGrid1.Row)) and
      (Selvalid = True) then
    begin

      Lokzuge := MlgZuge(DrawGrid1.Col, Drawgrid1.row);
      Zuge := Lokzuge;
    end;

  end;

  // Wenn zuvor Figur ausgewählt wurde und jetzt markiertes Feld (möglicher Zug)
  if auswahl.x <> 99 then
    if (Schachbrett[Auswahl.X, Auswahl.Y] is TFigur) and
      (Zuge[DrawGrid1.Col, DrawGrid1.Row] = True) and
      (((Zug = True) and (Auswahl.Farbe = 0)) or ((Zug = False) and
      (Auswahl.Farbe = 1))) then
    begin

      Figurbewegen(Drawgrid1.col, Drawgrid1.row);
      Schachmatt();
      if Zug then
        Panel1.Caption := 'Weiß ist am Zug!'
      else
        Panel1.Caption := 'Schwarz ist am Zug!';
    end;
  Figur := nil;
  Figur.Free;
end;


procedure TfrmSchach.Button1Click(Sender: TObject);
begin
  if (timer1.Enabled) and (ZeitW > 0) then
  begin
    timer1.Enabled := False;
    Button1.Caption := 'Start!';
    Drawgrid1.Enabled := False;
    Drawgrid1.Visible := False;
  end
  else
  begin
    Button1.Caption := 'Pause!';
    Drawgrid1.Enabled := True;
    Drawgrid1.Visible := True;

    timer1.Enabled := True;
  end;

end;

end.
