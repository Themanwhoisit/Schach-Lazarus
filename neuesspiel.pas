unit NeuesSpiel;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TFrmNeuesSpiel }

  TFrmNeuesSpiel = class(TForm)
    Button1: TButton;
    CheckBox1: TCheckBox;
    Edit1: TEdit;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
  private

  public

  end;

var
  FrmNeuesSpiel: TFrmNeuesSpiel;

implementation

uses Main;

{$R *.lfm}

{ TFrmNeuesSpiel }

procedure TFrmNeuesSpiel.CheckBox1Change(Sender: TObject);
begin
  edit1.enabled:=checkbox1.checked;
end;

procedure TFrmNeuesSpiel.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  Closeaction:=cafree;
end;

procedure TFrmNeuesSpiel.Button1Click(Sender: TObject);
var
  i: Integer;
begin
  i:=0;
  if checkbox1.checked then i:=StrToInt(edit1.text);

  Main.PbResetgame(i);
  Close;

end;

end.

